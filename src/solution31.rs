pub struct Solution;

impl Solution {
    pub fn next_permutation(nums: &mut Vec<i32>) {
        let len: i32 = nums.len() as i32;
        let mut index: i32 = 0;
        let mut temp: i32 = 0;
        let mut index_to_swap: i32 = 0;
        let mut i = 0;

        if len < 2 {
            return;
        }

        index = len - 2;
        while index >= 0 && nums[index as usize] >= nums[(index+1) as usize] {
            index -= 1;
        }

        if index < 0 {
            index = 0;
            while index < len / 2 {
                nums.swap(index as usize, (len - index - 1) as usize);
                index += 1;
            }
            return;
        }

        temp = nums[index as usize];
        index_to_swap = index + 1;

        i = index + 1;
        while i < len {
            if nums[i as usize] > temp {
                index_to_swap = i;
            }
            i += 1;
        }
        nums.swap(index as usize, index_to_swap as usize);

        i = 1;

        while index + i < len - i {
            nums.swap((index + i) as usize, (len - i) as usize);
            i += 1;
        }
        return;
    }
}