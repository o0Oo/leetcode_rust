use std::borrow::Borrow;

pub struct Solution{}

impl Solution {
    pub fn combination_sum(candidates: Vec<i32>, target: i32) -> Vec<Vec<i32>> {
        let mut cc = candidates;
        cc.sort();
        return Solution::rec(cc.borrow(), target, vec![]);
    }

    fn rec(arr: &Vec<i32>, target: i32, acc: Vec<i32>) -> Vec<Vec<i32>>{
        if arr.len() == 0 || arr[0] > target {
            return vec![];
        } else if arr[0] == target {
            let mut a = acc.clone();
            a.push(arr[0]);
            return vec![a];
        } else {
            let mut a = acc.clone();
            a.push(arr[0]);
            let fst = Solution::rec(arr, target - arr[0], a);
            let scd = Solution::rec(Vec::from(arr.split_first().unwrap().1).borrow(), target, acc);
            return vec![fst, scd].into_iter().flatten().collect();
        }
    }
}