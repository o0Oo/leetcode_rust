use std::borrow::BorrowMut;
use std::cmp;

pub struct Solution{}

impl Solution {
    pub fn trap(height: Vec<i32>) -> i32 {
        let len = height.len();
        let origin = height;
        let mut arr = vec![0; len];
        let mut i: usize = 0;
        let mut max = 0;
        while i < len {
            max = cmp::max(max, origin[i]);
            arr[i] = max;
            i += 1;
        }
        i = len - 1;
        max = 0;
        while i >= 1 {
            max = cmp::max(max, origin[i]);
            arr[i] = cmp::min(max, arr[i]);
            i -= 1;
        }
        let mut sum = 0;
        i = 0;
        while i < len {
            sum += arr[i] - origin[i];
            i += 1;
        }
        return sum;
    }
}