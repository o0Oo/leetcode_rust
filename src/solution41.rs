use std::borrow::BorrowMut;

pub struct Solution{}

impl Solution {
    pub fn first_missing_positive(nums: Vec<i32>) -> i32 {
        let mut arr = nums;
        let len = arr.len();
        let mut i: i32 = 0;
        let mut j: i32 = 0;
        while i < len as i32 {
            if arr[i as usize] == i + 1 {
                i += 1;
            } else if arr[i as usize] > 0 && arr[i as usize] <= (len as i32) {
                j = arr[i as usize] - 1;
                if arr[i as usize] == arr[j as usize] {
                    arr[i as usize] = 0;
                    i += 1;
                } else {
                    arr.swap(i as usize, j as usize);
                }
            } else {
                arr[i as usize] = 0;
                i += 1;
            }
        }
        i = 0;
        while i < len as i32 && arr[i as usize] == i + 1 {
            i += 1;
        }
        return i + 1;
    }

}