pub struct Solution{}

use std::borrow::BorrowMut;

impl Solution {
    pub fn solve_sudoku(board: &mut Vec<Vec<char>>) {
        let mut map: [i32;9] = [0,0,0,0,0,0,0,0,0];
        for i in 0..9 as usize {
            for j in 0..9 as usize {
                if board[i][j] != '.' {
                    let c = board[i][j] as usize - 49;
                    let sq = (i/3) * 3 + (j/3);
                    map[c] = map[c] | (1 << i + 1) | (1 << j + 10) | (1 << sq + 19);
                }
            }
        }
        Solution::rec(board.borrow_mut(), map.borrow_mut(), 0, 0, 0);
    }
    fn rec(board: &mut Vec<Vec<char>>,
           map: &mut [i32;9],
           c: usize,
           i: usize,
           j: usize) -> bool {
        if c >= 9 {
            return false;
        }
        if (i < 9) {
            if board[i][j] == '.' {
                let sq = (i/3) * 3 + (j/3);
                if map[c] & ((1 << i + 1) | (1 << j + 10) | (1 << sq + 19)) != 0 {
                    return Solution::rec(board.borrow_mut(), map.borrow_mut(), c + 1, i, j);
                } else {
                    map[c] = map[c] | (1 << i + 1) | (1 << j + 10) | (1 << sq + 19);
                    board[i][j] = (c + 49) as u8 as char;
                    if Solution::rec(board.borrow_mut(), map.borrow_mut(), 0, i + (j+1)/9, (j+1)%9) {
                        return true;
                    } else {
                        map[c] = map[c] ^ (1 << i + 1) ^ (1 << j + 10) ^ (1 << sq + 19);
                        board[i][j] = '.';
                        return Solution::rec(board.borrow_mut(), map.borrow_mut(), c + 1, i, j);
                    }
                }
            } else {
                return Solution::rec(board.borrow_mut(), map.borrow_mut(), 0, i + (j+1)/9, (j+1)%9);
            }
        } else {
            return true;
        }
    }
}