use std::borrow::Borrow;

pub struct Solution{}

impl Solution {
    pub fn count_and_say(n: i32) -> String {
        return Solution::rec(&vec![1], n-1).iter().map(ToString::to_string).collect();
    }

    fn rec(s: &Vec<i32>, n: i32) -> Vec<i32> {
        if n == 0 || s.len() == 0{
            return s.clone();
        } else {
            let mut str = vec![];
            let mut last = s[0];
            let mut count = 0;
            let mut i = 0;
            while i < s.len() {
                last = s[i];
                count = 0;
                while i < s.len() && last == s[i] {
                    count += 1;
                    i += 1;
                }
                str.push(count);
                str.push(last);
            }

            return Solution::rec(str.borrow(), n-1);
        }
    }
}