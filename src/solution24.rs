use crate::list::ListNode;

use core::option::Option;
use std::borrow::{Borrow, BorrowMut};
use std::ops::Deref;
use std::rc::Rc;
use std::cell::RefCell;


struct Solution;

// [problem!](https://leetcode.com/problems/reverse-nodes-in-k-group/)
impl Solution {


    pub fn reverse_k_group(head: Option<Box<ListNode>>, k: i32) -> Option<Box<ListNode>> {
        let o: Option<i32> = None;
        println!("{}", o.unwrap());
        head
        // let mut c = 1;
        //
        // let headMut: Rc<RefCell<Option<Box<ListNode>>>> = Rc::new(RefCell::new(head));
        // let headApNext: Rc<RefCell<Option<Box<ListNode>>>> = Rc::clone(&headMut);
        // let mut next: Rc<RefCell<Option<Box<ListNode>>>> = Rc::new(RefCell::new(None));
        // let mut i: Rc<RefCell<Option<Box<ListNode>>>> = Rc::clone(&headMut);
        //
        // let mut reverseCount = 0;
        // let mut  reverseHead: Rc<RefCell<Option<Box<ListNode>>>> = Rc::new(RefCell::new(None));
        // let mut  reverseAcc: Rc<RefCell<Option<Box<ListNode>>>> = Rc::clone(&reverseHead);
        // let mut  reverseTemp: Rc<RefCell<Option<Box<ListNode>>>> = Rc::clone(&reverseHead);
        //
        // let mut iter = Rc::try_unwrap(i).ok().unwrap().into_inner();
        //
        // while c < k && iter.is_some() {
        //     c += 1;
        //     // i.borrow_mut().replace_with(|&mut c| c.map(|b| b.next).flatten());
        //     let opt = iter.map(|a| a.next).flatten();
        //     i.replace(opt);
        // }
        //
        //
        // if let Some(a) = Rc::try_unwrap(i).ok().unwrap().into_inner() {
        //     Some(a)
        // } else {
        //     Rc::try_unwrap(next).ok().unwrap().into_inner()
        // }
    }
}

mod test {
    use crate::solution24::Solution;
    use crate::list::ListNode;

    #[test]
    fn test_case1() {
        let example = ListNode::from_vec(vec![1, 2, 3, 4, 5]);
        let expected = ListNode::from_vec(vec![2, 1, 4, 3, 5]);
        assert_eq!(Solution::reverse_k_group(example, 2), expected);
    }
    #[test]
    fn test_case2() {
        let example = ListNode::from_vec(vec![1, 2, 3, 4, 5]);
        let expected = ListNode::from_vec(vec![3, 2, 1, 4, 5]);
        assert_eq!(Solution::reverse_k_group(example, 3), expected);
    }
}