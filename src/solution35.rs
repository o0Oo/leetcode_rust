use std::borrow::Borrow;

pub struct Solution;

impl Solution {
    pub fn search_insert(nums: Vec<i32>, target: i32) -> i32 {
        return Solution::bsearch_fst(nums.borrow(), 0, nums.len() as i32 - 1, target);
    }

    fn bsearch_fst(nums: &Vec<i32>, from: i32, to:i32, target: i32) -> i32 {
        if target == nums[from as usize] {
            return from;
        } else if target < nums[from as usize] {
            return from;
        } else if target > nums[to as usize] {
            return to + 1;
        } else if from <= to {
            let mid = from + (to - from)/2;
            if nums[mid as usize] < target {
                return Solution::bsearch_fst(nums, mid + 1, to, target);
            } else {
                return Solution::bsearch_fst(nums, from + 1, mid, target);
            }
        } else {
            return -1;
        }
    }
}