use std::borrow::Borrow;
use std::cmp::max;

pub struct Solution;

impl Solution {
    pub fn search(nums: Vec<i32>, target: i32) -> i32 {
        match nums.len() {
            0 => return -1,
            1 => if nums[0] == target { return 0 } else { return -1 },
            _ => {
                let scnd = Solution::center(nums.borrow(), 0, (nums.len() - 1) as i32);
                println!("scnd {}", scnd);
                return max(
                    Solution::bsearch(nums.borrow(), 0, scnd - 1, target),
                    Solution::bsearch(nums.borrow(), scnd, (nums.len() - 1) as i32, target)
                );
            }
        }

    }

    fn bsearch(nums: &Vec<i32>, from: i32, to: i32, key: i32) -> i32 {
        if from < to {
            if nums[from as usize] == key {
                return from;
            } else if nums[to as usize] == key {
                return to;
            } else {
                let mid = from + (to - from) / 2;
                return max(
                  Solution::bsearch(nums, from, mid, key),
                  Solution::bsearch(nums, mid + 1, to, key)
                );
            }
        } else if from == to {
            if nums[from as usize] == key {
                return from;
            } else if nums[to as usize] == key {
                return to;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    fn center(nums: &Vec<i32>, from: i32, to: i32) -> i32 {
        if from < to {
            if from + 1 == to {
                return to;
            }
            let mid = from + (to-from + 1) / 2;
            if nums[mid as usize] > nums[to as usize] {
                return Solution::center(nums, mid, to);
            } else {
                return Solution::center(nums, from, mid);
            }
        } else {
            return 0;
        }
    }
}
