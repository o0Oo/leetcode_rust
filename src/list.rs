use std::slice::Iter;

#[derive(PartialEq, Eq, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        ListNode {
            next: None,
            val,
        }
    }

    pub fn from_vec(vec: Vec<i32>) -> Option<Box<ListNode>> {
        let reversed = {
            let mut res = vec.clone();
            res.reverse();
            res
        };
        reversed.iter()
            .fold(None,
                  |acc, el|
                     Some(Box::new(ListNode { val: *el, next: acc })) )
    }
}