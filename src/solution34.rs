use std::borrow::Borrow;

pub struct Solution;

impl Solution {
    pub fn search_range(nums: Vec<i32>, target: i32) -> Vec<i32> {
        match nums.len() {
            0 => return vec![-1, -1],
            1 => if nums[0] == target { return vec![0, 0] } else { return vec![-1,-1] },
            l => match Solution::bsearch_fst(nums.borrow(), 0, l as i32 -1, target) {
                -1 => return vec![-1,-1],
                i => {
                    println!("i : {:}", i);
                    vec![i, Solution::bsearch_scnd(nums.borrow(), i, l as i32 - 1, target)]
                }
            }
        }
    }
//vec![5,7,7,8,8,10]
    fn bsearch_fst(nums: &Vec<i32>, from: i32, to:i32, target: i32) -> i32 {
        if target == nums[from as usize] {
            return from;
        } else if target < nums[from as usize] || target > nums[to as usize] {
            return -1;
        } else if from <= to {
            let mid = from + (to - from)/2;
            if nums[mid as usize] < target {
                return Solution::bsearch_fst(nums, mid + 1, to, target);
            } else {
                return Solution::bsearch_fst(nums, from + 1, mid, target);
            }
        } else {
            return -1;
        }
    }

    fn bsearch_scnd(nums: &Vec<i32>, from: i32, to:i32, target: i32) -> i32 {
        if target == nums[to as usize] {
            return to;
        } else if target < nums[from as usize] || target > nums[to as usize] {
            return -1;
        } else if from <= to {
            let mid = from + (to - from)/2;
            if nums[mid as usize] <= target {
                return Solution::bsearch_scnd(nums, mid, to-1, target);
            } else {
                return Solution::bsearch_scnd(nums, from, mid-1, target);
            }
        } else {
            return -1;
        }
    }
}