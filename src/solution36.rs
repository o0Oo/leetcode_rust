
pub struct Solution {}

impl Solution {
    pub fn is_valid_sudoku(board: Vec<Vec<char>>) -> bool {
        let mut map: [i32;9] = [0,0,0,0,0,0,0,0,0];
        let mut c: usize = 0;
        let mut sq: usize = 0;
        let mut i: usize = 0;
        let mut j: usize = 0;
        while i < 9 {
            j = 0;
            while j < 9 {
                if board[i][j] != '.' {
                    c = board[i][j] as usize - 49;
                    sq = (i/3) * 3 + (j/3);
                    if map[c] & ((1 << i + 1) | (1 << j + 10) | (1 << sq + 19)) != 0 {
                        return false;
                    } else {
                        map[c] = map[c] | (1 << i + 1) | (1 << j + 10) | (1 << sq + 19);
                    }
                }
                j = j + 1;
            }
            i = i + 1;
        }
        return true;
    }
}