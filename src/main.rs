
use std::borrow::{Borrow, BorrowMut};

mod solution39;
mod solution38;
mod solution37;
mod solution36;
mod solution35;
mod solution34;
mod solution33;
mod solution31;
mod list;
mod solution40;
mod solution41;
mod solution42;

fn main() {

    // let test1 = solution33::Solution::search(vec![4, 5, 6, 7, 0, 1, 2], 0);
    // println!("{:}", test1);
    // let test2 = solution33::Solution::search(vec![1], 1);
    // println!("{:}", test2);
    // let test3 = solution33::Solution::search(vec![1, 3], 1);
    // println!("{:}", test3);
    // let test1 = solution34::Solution::search_range(vec![5,7,7,8,8,10], 8);
    // println!("{:?}", test1);
    // let test2 = solution34::Solution::search_range(vec![5,7,7,8,8,10], 6);
    // println!("{:?}", test2);
    // let test3 = solution34::Solution::search_range(vec![], 0);
    // println!("{:?}", test3);
    // let test4 = solution34::Solution::search_range(vec![1,2,3], 1);
    // println!("{:?}", test4);
    // let test5 = solution34::Solution::search_range(vec![0,0,1,2,2], 0);
    // println!("{:?}", test5);
    // let test1 = solution36::Solution::is_valid_sudoku(vec![vec!['5','3','.','.','7','.','.','.','.'],vec!['6','.','.','1','9','5','.','.','.'],vec!['.','9','8','.','.','.','.','6','.'],vec!['8','.','.','.','6','.','.','.','3'],vec!['4','.','.','8','.','3','.','.','1'],vec!['7','.','.','.','2','.','.','.','6'],vec!['.','6','.','.','.','.','2','8','.'],vec!['.','.','.','4','1','9','.','.','5'],vec!['.','.','.','.','8','.','.','7','9']]);
    // println!("{:?}", test1);
    // let test2 = solution36::Solution::is_valid_sudoku(vec![vec!['.','.','.','9','.','.','.','.','.'],vec!['.','.','.','.','.','.','.','.','.'],vec!['.','.','3','.','.','.','.','.','1'],vec!['.','.','.','.','.','.','.','.','.'],vec!['1','.','.','.','.','.','3','.','.'],vec!['.','.','.','.','2','.','6','.','.'],vec!['.','9','.','.','.','.','.','7','.'],vec!['.','.','.','.','.','.','.','.','.'],vec!['8','.','.','8','.','.','.','.','.']]);
    // println!("{:?}", test2);

    // let mut test1 = vec![vec!['5','3','.','.','7','.','.','.','.'],vec!['6','.','.','1','9','5','.','.','.'],vec!['.','9','8','.','.','.','.','6','.'],vec!['8','.','.','.','6','.','.','.','3'],vec!['4','.','.','8','.','3','.','.','1'],vec!['7','.','.','.','2','.','.','.','6'],vec!['.','6','.','.','.','.','2','8','.'],vec!['.','.','.','4','1','9','.','.','5'],vec!['.','.','.','.','8','.','.','7','9']];
    // solution37::Solution::solve_sudoku(test1.borrow_mut());
    // println!("{:?}", test1);

    // let test1 = solution38::Solution::count_and_say(1);
    // assert_eq!(test1, "1211");

    // let test1 = solution39::Solution::combination_sum(vec![2,3,5], 8);
    // assert_eq!(test1, vec![vec![2,2,2,2],vec![2,3,3],vec![3,5]]);

    // let test1 = solution41::Solution::first_missing_positive(vec![1,2,0]);
    // assert_eq!(test1, 3);
    // let test2 = solution41::Solution::first_missing_positive(vec![3,4,-1,1]);
    // assert_eq!(test2, 2);
    // let test3 = solution41::Solution::first_missing_positive(vec![0,1,2]);
    // assert_eq!(test3, 3);
    // let test4 = solution41::Solution::first_missing_positive(vec![1]);
    // assert_eq!(test4, 2);
    // let test5 = solution41::Solution::first_missing_positive(vec![1, 1]);
    // assert_eq!(test5, 2);

    let test1 = solution42::Solution::trap(vec![0,1,0,2,1,0,1,3,2,1,2,1]);
    assert_eq!(test1, 6);
    let test2 = solution42::Solution::trap(vec![4,2,0,3,2,5]);
    assert_eq!(test2, 9);


}
