use std::borrow::{Borrow, BorrowMut};

pub struct Solution{}

impl Solution {
    pub fn combination_sum2(candidates: Vec<i32>, target: i32) -> Vec<Vec<i32>> {
        let mut arr= vec![0; 30];
        for i in candidates {
            if i <= 30 {
                arr[(i as usize)-1] += 1;
            }
        }
        return Solution::rec(arr.borrow_mut(), target-1, target, vec![]);
    }

    fn rec(arr: &mut Vec<i32>, i: i32, target: i32, acc: Vec<i32>) -> Vec<Vec<i32>>{
        if target == 0 {
            return vec![acc];
        } else if i < 0 {
            return vec![];
        } else if i+1 > target || arr[i as usize] == 0 {
            return Solution::rec(arr.borrow_mut(), i - 1, target, acc);
        } else {
            arr[i as usize] -= 1;
            let mut acc2 = vec![i+1];
            acc2.append(acc.clone().borrow_mut());
            let mut h = Solution::rec(arr.borrow_mut(), i, target - (i+1), acc2);
            arr[i as usize] += 1;
            let mut t = Solution::rec(arr.borrow_mut(), i-1, target, acc);
            h.append(t.borrow_mut());
            return h;
        }
    }
}